package com.nlmk.sychikov.tm.dao;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Projects storage
 */
public class ProjectDAO {

    private List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public Project findByIndex(int index) {
        if (index > projects.size() - 1 || index < 0) return null;
        return projects.get(index);
    }

    public Project findById(Long id) {
        if (id == null) return null;
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public List<Project> findAll() {
        return projects;
    }

    public static void main(String[] args) {
        final ProjectDAO projectDAO = new ProjectDAO();
        projectDAO.create("TEST");
        System.out.println(projectDAO.findAll());
    }

}
