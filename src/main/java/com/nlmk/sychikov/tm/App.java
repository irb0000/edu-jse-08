package com.nlmk.sychikov.tm;

import com.nlmk.sychikov.tm.dao.ProjectDAO;
import com.nlmk.sychikov.tm.dao.TaskDAO;
import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;

import java.util.Scanner;

import static com.nlmk.sychikov.tm.constant.TerminalConst.*;

/**
 * Task manager
 *
 * @author Irb
 */
public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    static {
        projectDAO.create("Test project 1");
        projectDAO.create("Test project 2");
        final Task task = taskDAO.create("Task demo 1");
        task.setDescription("Description 1");
        taskDAO.create("Task demo 2");
    }

    /**
     * Main (start point)
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        run(args);
        displayWelcome();
        process();
    }

    /**
     * Command line args processor
     *
     * @param args command line arguments
     */
    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * One command processor
     *
     * @param param command
     * @return return value
     */
    private static int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return exit();

            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_VIEW_BY_INDEX:
                return viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return viewProjectById();
            case PROJECT_REMOVE_BY_ID:
                return removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return removeProjectByName();
            case PROJECT_UPDATE_BY_INDEX:
                return updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return updateProjectById();

            case TASK_CLEAR:
                return clearTask();
            case TASK_CREATE:
                return createTask();
            case TASK_LIST:
                return listTask();
            case TASK_VIEW_BY_INDEX:
                return viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return viewTaskById();
            case TASK_REMOVE_BY_ID:
                return removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName();
            case TASK_UPDATE_BY_INDEX:
                return updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return updateTaskById();

            default:
                return displayError();
        }
    }

    /**
     * Add new project
     *
     * @return return value
     */
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectDAO.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by id
     *
     * @return return value
     */
    private static int updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        final Long id = idInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectDAO.findById(id);
        if (project == null) {
            System.out.println("[Project id='" + id.toString() + "' is not found!]");
            return 0;
        }
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectDAO.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update project by index
     *
     * @return return value
     */
    private static int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY ID]");
        final int index = indexInputProcessor("Enter project id: ");
        if (index == -1)
            return 0;
        final Project project = projectDAO.findByIndex(index);
        if (project == null) {
            System.out.println("[Project with index='" + index + "' is not found!]");
            return 0;
        }
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter project description: ");
        final String description = scanner.nextLine();
        projectDAO.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by name
     *
     * @return return value
     */
    private static int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if (project == null) System.out.println("[Project '" + name + "' is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by id
     *
     * @return return value
     */
    private static int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        final Long id = idInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectDAO.removeById(id);
        if (project == null) System.out.println("[Project with id=" + id.toString() + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes project by index
     *
     * @return return value
     */
    private static int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        final int index = indexInputProcessor("Enter project index: ");
        if (index == -1)
            return 0;
        final Project project = projectDAO.removeByIndex(index);
        if (project == null)
            System.out.println("[Project with index=" + index + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all projects
     *
     * @return return value
     */
    private static int clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects
     *
     * @return return value
     */
    private static int listProject() {
        System.out.println("[LIST PROJECTS]");
        int index = 1;
        for (Project project : projectDAO.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + ": " + project.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the project
     *
     * @param project what project should be printed
     */
    private static void viewProject(final Project project) {
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows project by index
     *
     * @return return value
     */
    private static int viewProjectByIndex() {
        final int index = indexInputProcessor("Enter project index: ");
        if (index == -1)
            return 0;
        final Project project = projectDAO.findByIndex(index);
        viewProject(project);
        return 0;
    }

    /**
     * Shows project by id
     *
     * @return return value
     */
    private static int viewProjectById() {
        final Long id = idInputProcessor("Enter project id: ");
        if (id == -1)
            return 0;
        final Project project = projectDAO.findById(id);
        if (project == null) {
            System.out.println("[Project id='" + id.toString() + "' is not found!]");
            return 0;
        }
        viewProject(project);
        return 0;
    }

    /**
     * Add new task
     *
     * @return return value
     */
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskDAO.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update task by index
     *
     * @return return value
     */
    private static int updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        final int index = indexInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskDAO.findByIndex(index);
        if (task == null) {
            System.out.println("[Task index='" + index + "' is not found!]");
            return 0;
        }
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskDAO.update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Update task by id
     *
     * @return return value
     */
    private static int updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        final Long id = idInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskDAO.findById(id);
        if (task == null) {
            System.out.println("[Task id='" + id.toString() + "' is not found!]");
            return 0;
        }
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        System.out.print("Enter task description: ");
        final String description = scanner.nextLine();
        taskDAO.update(id, name, description);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by name
     *
     * @return return value
     */
    private static int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null) System.out.println("[Task '" + name + "' is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by id
     *
     * @return return value
     */
    private static int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        final Long id = idInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskDAO.removeById(id);
        if (task == null)
            System.out.println("[Task with id=" + id.toString() + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes task by index
     *
     * @return return value
     */
    private static int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        final int index = indexInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskDAO.removeByIndex(index);
        if (task == null)
            System.out.println("[Task with index=" + index + " is not found!]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all tasks
     *
     * @return return value
     */
    private static int clearTask() {
        System.out.println("[CLEAR TASKS]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Show the task
     *
     * @param task what task should be printed
     */
    private static void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASKS]");
        System.out.println("ID:" + task.getId());
        System.out.println("NAME:" + task.getName());
        System.out.println("DESCRIPTION:" + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Shows task by index
     *
     * @return return value
     */
    private static int viewTaskByIndex() {
        final int index = indexInputProcessor("Enter task index: ");
        if (index == -1)
            return 0;
        final Task task = taskDAO.findByIndex(index);
        if (task == null)
            System.out.println("Task with index=" + index + " is not found!");
        else viewTask(task);
        return 0;
    }

    /**
     * Shows task by id
     *
     * @return return value
     */
    private static int viewTaskById() {
        final Long id = idInputProcessor("Enter task id: ");
        if (id == -1)
            return 0;
        final Task task = taskDAO.findById(id);
        if (task == null)
            System.out.println("Task with index=" + id.toString() + " is not found!");
        else viewTask(task);
        return 0;
    }

    /**
     * List all tasks
     *
     * @return return value
     */
    private static int listTask() {
        System.out.println("[LIST TASKS]");
        int index = 1;
        for (Task task : taskDAO.findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName() + ": " + task.getDescription());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Infinite loop command processor with console input.
     * The "exit" command stops the loop.
     */
    private static void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            System.out.print("command:> ");
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Show welcome string
     */
    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Show available command
     *
     * @return return value
     */
    private static int displayHelp() {
        System.out.println(VERSION + " - display the version info");
        System.out.println(HELP + " - display the list of terminal commands");
        System.out.println(ABOUT + " - display the developer info");
        System.out.println(EXIT + " - close the program");
        System.out.println();
        System.out.println(PROJECT_CREATE + " - create the new project");
        System.out.println(PROJECT_CLEAR + " - clear all projects");
        System.out.println(PROJECT_LIST + " - list all projects");
        System.out.println(PROJECT_VIEW_BY_INDEX + " - shows project by  index");
        System.out.println(PROJECT_VIEW_BY_ID + " - shows project by id");
        System.out.println(PROJECT_UPDATE_BY_INDEX + " - updates project by index");
        System.out.println(PROJECT_UPDATE_BY_ID + " - updates project by id");
        System.out.println(PROJECT_REMOVE_BY_INDEX + " - removes project by index");
        System.out.println(PROJECT_REMOVE_BY_ID + " - removes project by id");
        System.out.println(PROJECT_REMOVE_BY_NAME + " - removes project by name");
        System.out.println();
        System.out.println(TASK_CREATE + " - create the new task");
        System.out.println(TASK_CLEAR + " - clear all tasks");
        System.out.println(TASK_LIST + " - list all tasks");
        System.out.println(TASK_VIEW_BY_INDEX + " - shows task by index");
        System.out.println(TASK_VIEW_BY_ID + " - shows task by id");
        System.out.println(TASK_UPDATE_BY_INDEX + " - updates task index");
        System.out.println(TASK_UPDATE_BY_ID + " - updates task by id");
        System.out.println(TASK_REMOVE_BY_INDEX + " - removes task by index");
        System.out.println(TASK_REMOVE_BY_ID + " - removes task by id");
        System.out.println(TASK_REMOVE_BY_NAME + " - removes task by name");
        return 0;
    }

    /**
     * Show error message
     *
     * @return return value
     */
    private static int displayError() {
        System.out.println("Error! Unknown command...");
        return -1;
    }

    /**
     * Show version
     *
     * @return return value
     */
    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Show exit string
     *
     * @return value
     */
    private static int exit() {
        System.out.println("Our program exit now...");
        System.out.println("Bye!");
        return 0;
    }

    /**
     * Show info
     *
     * @return return value
     */
    private static int displayAbout() {
        System.out.println("Vladimir Sychikov");
        System.out.println("VladimirSychikov@nospam.ru");
        return 0;
    }

    /**
     * Index input processing with error message when not a number or empty value
     * or negative value received
     *
     * @return entered int value or -1 (when input is wrong)
     */
    private static int indexInputProcessor(final String inputPrompt) {
        System.out.print(inputPrompt);
        final String stringValue = scanner.nextLine();
        try {
            final int i = Integer.parseInt(stringValue);
            if (i > 0)
                return i - 1;
            else
                System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return -1;
        } catch (NumberFormatException e) {
            System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return -1;
        }
    }

    /**
     * Id Long input processing with error message when not a number or empty value
     * or negative value received
     *
     * @return entered Long value or -1 (when input is wrong)
     */
    private static Long idInputProcessor(String inputPrompt) {
        System.out.print(inputPrompt);
        final String stringValue = scanner.nextLine();
        try {
            final Long l = Long.getLong(stringValue);
            if (l > 0)
                return l;
            else
                System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return (long) -1;
        } catch (NumberFormatException e) {
            System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return (long) -1;
        }
    }

}
